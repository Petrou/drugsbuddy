# Drugbuddy.org
This is the repository of our django based harm reduction site, the frontend for the entire platform.
The code in this repository will communicate with the various microservices we will set up, and will process the requested data for the users of the site.

## Current microservices
 - toleranceCalculator:
  This microservice will calculate all tolerances and will display or process the data on the users profile or calculator page to show a estimated ideal dose.

## Wiki
This is also the place where you will find the wiki for the entire project, you can find and or contribute to it on the left hand site of the repository.

## Development guide
To commit or push changes you will first have to enter the `development` branch and from there branch of to your own branch, after your changes have been completed you can submit a merge request to mere your branch to `development` this request will be reviewed and merged if accepted, all merge conflicts will be handled and reviewed by the board.
