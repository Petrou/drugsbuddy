from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
    html = "\
     <html>\
        <header>\
            <title> DrugsBuddy.org </title>\
			  <meta charset=UTF-8'>\
			  <meta name='description' content='This site aims at the harm reduction of psychoactive subtances for recreational drug users.'>\
			  <meta name='keywords' content='HTML,CSS,Python,site,harm,reduction,psychoactive,drugs,advise,dosage,AI,tripsitter,safety,calculator,LSD'>\
			  <meta name='author' content='Iakovos Petrou'>\
			  <meta name='viewport' content='width=device-width, initial-scale=1.0'>\
			  <meta name='google-site-verification' content='JseqOj1vL9mzBSDoxPDn3IKIDGjYSeQmn7XEveWs48Y' />\
           <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet'> \
		   </header>\
           <style>\
               .title{\
                    color: white;\
                    font-family: 'Oswald', sans-serif;\
                    margin-top: 5%;\
                    line-height: 15px;\
                }\
                html{\
                    height: 100%;\
                    background-image: radial-gradient(purple, black);\
                    }\
                .paragraphs{\
                    line-height: 2px;\
                    }\
                .footer{\
                    position: absolute;\
                    bottom: 0;\
                    color: white;\
                    }\
                .aboutHead{\
                    color:black;\
                    border: 5px solid black;\
                    background: white;\
                    border-radius: 14px 14px 14px 14px;\
                    font-family: 'Oswald', sans-serif;\
                    width: 40%;\
                    line-height: 10px;\
                    background-image: linear-gradient(white, gray);\
                    }\
                .aboutBody{\
                    color:black;\
                    border: 5px solid black;\
                    background: white;\
                    border-radius: 14px 14px 14px 14px;\
                    font-family: 'Oswald', sans-serif;\
                    width: 40%;\
                    line-height: 20px;\
                    background-image: linear-gradient(white, gray);\
                    }\
                .aboutBody p{\
                    text-align: left;\
                }\
                .aboutBody ul{\
                    text-align: left;\
                }\
           </style>\
            <body>\
            <center>\
                <div class='title'>\
                    <h1>Welcome to Drugbuddy.org</h1>\
                    <h1>This site is currently under contruction!</h1>\
                    <div class ='paragraphs'>\
                        <br>\
                        <p> Everything on this site is advice.</p>\
                        <p> meaning that everything is an <u>approximation</u>. </p>\
                        <p> You are always responisble for your own <u>safety!</u> </p>\
                    <div>\
                </div>\
                </center>\
                <center>\
                <div class='aboutHead'>\
                    <h2>Welcome to Drugbuddy.org</h2>\
                    <p>This site aims at the harm reduction of psychoactive subtances for recreational drug users.</p>\
                    <p>Currently we are under constrution, but for now you can take a look at all the features we are</p>\
                    <p>going to implement into the site.</p>\
                </div>\
                    <br>\
                <div class= 'aboutBody'>\
                    <h2>Features:</h2>\
                    <p>-Automatic tolerance calculations based on your last dosage.</p>\
                    <p>-A social profile where you can keep track of your dosages and trip reports.</p>\
                    <p>-A site intergrated IRC chat where you can chat with other users or request assistance.</p>\
                    <br>\
                    <p>-A video service that allows you to connect to a tripsitter if you are in need of a more human experience.</p>\
                    <ul>\
                        <li>There will be an AI analyzing the session to train and ensure optimal assistance.</li>\
                        <li>This AI will analyze the users face to detect mood, set, setting and sounds.</li>\
                        <li>It will automatically transcode the audio to text and look for key/trigger words to inform the tripsitter of the best course of action and call for extra assistence if needed, ensuring you will have the best experience in your trip</li>\
                    </ul>\
                    <br>\
                    <p>-An extensive wiki containing all the information on substances you need.</p>\
                    <p>-A mobile app that contains all tolerance calculators and intergration with the platform, for easy dosage registration.</p>\
                    <p>-Information on combinations of various substances.</p>\
                    <p>-Safe usage information when preparing for taking a substance.</p>\
                </div>\
            <div class='footer'>\
                <h5> Jeroen Mathon, Iakovos Petrou, Maruwan, Blissfull;</h5>\
            </div>\
        </body>\
    </html>"
    return HttpResponse(html)
