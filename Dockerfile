FROM python:3
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /code
 WORKDIR /code/
 ADD ./requirements.txt /code/
 ADD ./drugbuddy/ /code/
 RUN pip install -r requirements.txt
 EXPOSE 80
 #CMD ["python", "/code/drugbuddy/manage.py", "runserver", "0.0.0.0:80"]
 CMD exec gunicorn drugbuddy.wsgi:application --bind 0.0.0.0:80 --workers 3
